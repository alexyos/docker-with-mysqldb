import mysql.connector as mysql
from Data.db_info import DbData

db = mysql.connect(host="localhost",
                   port=3306,
                   user=DbData.db_user(),
                   passwd=DbData.db_passwd(),
                   database=DbData.db_name())
cursor = db.cursor()


def show():
    query = "SHOW TABLES"
    cursor.execute(query)
    print(cursor.fetchall())
    db.close()


def all_veh_with_license_plate():
    query = "SELECT Make, Model, Registration FROM vehicles"
    cursor.execute(query)
    print("\nAll vehicles with registration:\n===============================")
    for i in cursor:
        print(f"{i[0]} {i[1]}: licence plate - {i[2]}")


def all_veh_without_selected(make):
    query = "SELECT Make, Model, Registration FROM vehicles"
    cursor.execute(query)
    c = []
    print("\nSelected vehicles\n======================")
    for i in cursor:
        if i[0] != make:
            print(f"{i[0]} {i[1]}: licence plate - {i[2]}")
        else:
            c.append(i)
    if not c:
        print("\nSelected vehicle not found")


def all_veh_mileage(mn, mx):
    query = "SELECT Make, Model, Mileage, Registration FROM vehicles"
    cursor.execute(query)
    v = []
    print("\nVehicles with selected mileage:\n===============================")

    for i in cursor:
        if mn <= i[2] <= mx:
            print(f"{i[0]} {i[1]}: licence plate - {i[3]} mileage={i[2]}")
            v.append(i)
    if not v:
        print("\nNo vehicles found")


def all_customers():
    query = "SELECT Name FROM customers"
    cursor.execute(query)
    for i in cursor:
        print(*i)
    cursor.execute(query)
    print("\nTotal customers count: ", len(cursor.fetchall()))


def add_cars(value):
    query = "INSERT INTO vehicles (Make, Model, Transmission, Drivetrain, " \
            "Category, Mileage, Registration, Price) " \
            "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
    cursor.executemany(query, value)
    db.commit()
    print("Cars successfully added...")
    print(all_veh_with_license_plate())


def add_discount():
    cursor.execute("ALTER TABLE customers ADD COLUMN Discount "
                   "FLOAT UNSIGNED")

    value = ([0.08, 1001], [0.05, 1003], [0.09, 1005], [0.12, 1007],
             [0.15, 1009], [0.18, 1002], [0.17, 1004], [0.11, 1006],
             [0.19, 1008], [0.13, 1010])
    for i in value:
        cursor.execute("UPDATE customers SET Discount = %s WHERE ID = %s", i)
    db.commit()
    query1 = "SELECT ID, Name, Discount FROM customers"
    cursor.execute(query1)
    for i in cursor:
        print(f"{i[0]} {i[1]} is having {i[2] * 100}% Discount")


def top_discount():
    query = "SELECT Name, ID, Discount FROM customers " \
            "ORDER BY Discount desc limit 0,3"
    cursor.execute(query)
    print("\nTop 3 Discount customers:\n=========================")
    for i in cursor:
        print(f"{i[1]}, {i[0]} is having {i[2]*100}% discount")


# db.close()

# show()

# all_veh_with_license_plate()

# all_veh_without_selected("Nissan")

# all_veh_mileage(100000, 150000)

# all_customers()

# add_cars([('BMW', 'X5', 'Auto', 'A', 'SUV', 58200, 'B1045Y', 69.5),
#           ('Lada', 'XZC', 'Auto', 'F', 'Sedan', 119070, 'X1391C', 22.2)])

# add_discount()

top_discount()
