import mysql.connector as mysql
from Data.db_info import DbData
import subprocess
import docker
import time

process = subprocess.Popen(['docker pull mysql:8'],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE,
                           shell=True,
                           universal_newlines=True)

while True:
    output = process.stdout.readline()
    print(output.strip())
    return_code = process.poll()
    if return_code is not None:
        print('Image pulled...', return_code)
        break

client = docker.from_env()

client.containers.run('mysql:8', name='dcrmysql', environment=[
                      'MYSQL_ROOT_PASSWORD=' + DbData.db_passwd()],
                      ports={'3306/tcp': 3306}, detach=True)
print("Setting up container with initial database...")

a = []
for container in client.containers.list():
    a.append(container.id)
for container in client.containers.list():
    container.stop()

time.sleep(2)

process = subprocess.Popen(['docker', 'container', 'start', a[0]],
                           stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE,
                           text=True)
while True:
    output = process.stdout.readline()
    print(output.strip())
    return_code = process.poll()
    if return_code is not None:
        print('Restarting container success...', return_code)
        break
time.sleep(5)

print("Creating Carshare Database...")
db = mysql.connect(host="localhost",
                   port=3306,
                   user=DbData.db_user(),
                   passwd=DbData.db_passwd())
cursor = db.cursor()
cursor.execute("CREATE DATABASE carshare_db")

db = mysql.connect(host="localhost",
                   port=3306,
                   user=DbData.db_user(),
                   passwd=DbData.db_passwd(),
                   database=DbData.db_name())
cursor = db.cursor()
#
cursor.execute("CREATE TABLE vehicles ("
               "Make VARCHAR(30) NOT NULL, "
               "Model VARCHAR(30) NOT NULL, "
               "Transmission VARCHAR(10) NOT NULL, "
               "Drivetrain VARCHAR(3) NOT NULL, "
               "Category VARCHAR(20) NOT NULL, "
               "Mileage INT UNSIGNED NOT NULL, "
               "Registration VARCHAR(30) UNIQUE KEY NOT NULL, "
               "Price FLOAT NOT NULL)")

cursor.execute("CREATE TABLE customers ("
               "ID INT(11) AUTO_INCREMENT PRIMARY KEY, "
               "Name VARCHAR(50) NOT NULL, "
               "Address VARCHAR(255) NOT NULL, "
               "Phone VARCHAR(20) NOT NULL)")

cursor.execute("ALTER TABLE customers AUTO_INCREMENT=1001")

cursor.execute("CREATE TABLE history ("
               "Rent_ID INT(11) AUTO_INCREMENT PRIMARY KEY, "
               "User_ID INT(11), FOREIGN KEY(User_ID) REFERENCES customers("
               "ID), "
               "Registration VARCHAR(30), FOREIGN KEY(Registration) "
               "REFERENCES vehicles(Registration), "
               "Start DATE NOT NULL, "
               "End DATE NOT NULL)")

cursor.execute("ALTER TABLE history AUTO_INCREMENT=2001")

query = "INSERT INTO vehicles (Make, Model, Transmission, Drivetrain, " \
        "Category, Mileage, Registration, Price) " \
        "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

query1 = "INSERT INTO customers (Name, Address, Phone) VALUES (%s, %s, %s)"

query2 = "INSERT INTO history (User_ID, Registration, Start, End) " \
         "VALUES (%s, %s, %s, %s)"

cursor.executemany(query, DbData.vehicle_data())
cursor.executemany(query1, DbData.client_data())
cursor.executemany(query2, DbData.history_data())
db.commit()
print("\nDatabase tables have been created and ready for SQL query.... ")
