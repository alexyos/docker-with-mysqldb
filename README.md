# README #

This is simple example of Docker with python.
Docker pulls MySQL image builds container and populates it data (in this
 instance we use example of car sharing service)


### How do I get set up? ###

* By running carsharing.py docker will create a container with database and
 python will fill tables with sample data about users, cars and history rent
 
* In db-selectors.py are functions that can be used to make different queries
 to the database and pull needed data.

